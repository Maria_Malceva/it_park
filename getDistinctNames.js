/**
 * Returns an array of distinct node names met in the specified tree.
 *
 * @param {Object} rootNode Root node of the tree.
 * @param {Array<string>} set Array of node names of the tree.
 *
 * @example
 *      getDistinctNames({
 *          name: "abc",
 *          nodes: [
 *              {
 *                  name: "name-1",
 *              }
 *          ]
 *      });
 * @returns {Array<string>} Array of distinct names ordered alphabetically.
 */
 
/* final version */ 
 function getDistinctNames(rootNode) {
	let set = new Set();
	
	if (rootNode.nodes !== '' && rootNode.nodes !== undefined) {
		rootNode.nodes.forEach((node) => {
			set = new Set([...set, ...getDistinctNames(node)]);	
		});	
	}
	
	set.add(rootNode.name);
	return ([...set].sort());
}
 
/*first version
 let arr = []; // массив уникальных имен узлов дерева
 
function getDistinctNames(rootNode) {
if (arr.indexOf(rootNode.name) == -1) // если имя узла не встречалось ранее, то
		arr.push(rootNode.name);	// добавляем его в массив уникальных имен узлов
    if (rootNode.nodes != '' && rootNode.nodes != undefined) {
		for (let node of rootNode.nodes) {	// для каждого потомка вызываем функцию получения имени
			getDistinctNames(node);
		}
	}
    return (arr.sort());	// сортируем полученный массив имен узлов
}
*/

/*version2 */
/*
function getDistinctNames(rootNode) {
    let arr = [];
    if (rootNode.nodes != '' && rootNode.nodes != undefined) {
        for (let node of rootNode.nodes) { // для каждого потомка вызываем функцию получения имени
            arr = arr.concat(getDistinctNames(node));
        }
    }
    if (arr.indexOf(rootNode.name) == -1) // если имя узла не встречалось ранее, то
        arr.push(rootNode.name); // добавляем его в массив уникальных имен узлов	
    arr = arr.filter(function(item, pos) { // исключаем повторяющиеся имена узлов
        return arr.indexOf(item) == pos;
    })
    return (arr.sort()); // сортируем полученный массив имен узлов
}
*/

module.exports = getDistinctNames;
