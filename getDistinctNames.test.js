const assert = require("assert");
const getDistinctNames = require("./getDistinctNames");

describe("getDistinctNames", () => {

    it("excludes repeated names", () => {
        const root = {
            name: "1",
            nodes: [
                {
                    name: "1",
                    nodes: [
                        {
                            name: "1"
                        }
                    ]
                },
                {
                    name: "1",
                    nodes: [
                        {
                            name: "1"
                        }
                    ]
                }
            ]
        };

        assert.deepEqual(getDistinctNames(root), ["1"]);
    });

	it("returns array of node names including the root name", () => {
        const root = {
            name: "1",
            nodes: []
        };

        assert.deepEqual(getDistinctNames(root), ["1"]);
    });

    it("includes names of root immediate children", () => {
        const root = {
            name: "1",
            nodes: [
                {
                    name: "2"
                },
                {
                    name: "3"
                }
            ]
        };

        assert.deepEqual(getDistinctNames(root), ["1", "2", "3"]);
    });

    it("includes names of all root descendants", () => {
        const root = {
            name: "1",
            nodes: [
                {
                    name: "2",
                    nodes: [
                        {
                            name: "4"
                        }
                    ]
                },
                {
                    name: "3",
                    nodes: [
                        {
                            name: "5"
                        }
                    ]
                }
            ]
        };

        assert.deepEqual(getDistinctNames(root), ["1", "2", "3", "4", "5"]);
    });

    it("orders names alphabetically", () => {
        const root = {
            name: "4",
            nodes: [
                {
                    name: "3",
                    nodes: [
                        {
                            name: "5"
                        }
                    ]
                },
                {
                    name: "1",
                    nodes: [
                        {
                            name: "2"
                        }
                    ]
                }
            ]
        };

        assert.deepEqual(getDistinctNames(root), ["1", "2", "3", "4", "5"]);
    });

    it("does not duplicate previous result", () => {
		const root = {
            name: "3",
            nodes: [
                {
                    name: "1",
                    nodes: [
						{
                            name: "5"
                        }
                    ]
                },
                {
                    name: "1",
                    nodes: [
                        {
                            name: "2"
                        }
                    ]
                }
            ]
        };
 
		const root1 = {
            name: "9",
            nodes: [
                {
                    name: "8",
                    nodes: [
                        {
                            name: "6"
                        }
                    ]
                },
                {
                    name: "7",
                    nodes: [
                        {
                            name: "4"
                        }
                    ]
                }
            ]
        };
	assert.deepEqual(getDistinctNames(root), ["1", "2", "3", "5"]);
	assert.deepEqual(getDistinctNames(root1), ["4", "6", "7", "8", "9"]);
	});
	
    const generateNames = n => new Array(n).fill(0).map((_1, i) => String(i));

    it.skip("handles huge trees of samely named nodes in a reasonable time", () => {
        const names = new Array(1000000).fill("0");
        const rootNode = {
            name: "0",
            nodes: names.map(name => ({ name }))
        };

        const start = new Date();
        const actualNames = getDistinctNames(rootNode);
        const end = new Date();
        const executionTime = end - start;

        assert.deepEqual(actualNames, ["0"]);
        assert(executionTime < 1000, `execution took ${executionTime}ms`);
    });

    it.skip("handles huge trees in a reasonable time", () => {
        const names = generateNames(100000);
        const rootNode = {
            name: "0",
            nodes: names.map(name => ({ name }))
        };

        const start = new Date();
        const actualNames = getDistinctNames(rootNode);
        const end = new Date();
        const executionTime = end - start;

        assert.deepEqual(actualNames, names.sort());
        assert(executionTime < 1000, `execution took ${executionTime}ms`);
    });

    it.skip("handles trees of a great height in a reasonable time", () => {
        const names = generateNames(100000);
        const rootNode = names.reduce((child, name) => {
            return {
                name,
                nodes: child === null ? [] : [child]
            };
        }, null);

        const start = new Date();
        const actualNames = getDistinctNames(rootNode);
        const end = new Date();
        const executionTime = end - start;

        assert.deepEqual(actualNames, names.sort());
        assert(executionTime < 1000, `execution took ${executionTime}ms`);
    });
});
